<?php

namespace app\models\activeRecord;

/**
 * This is the model class for table "movie".
 *
 * @property int $id
 * @property string $title
 * @property string $original_title
 * @property string $release_date
 * @property int $runtime
 * @property string $overview
 * @property string $genres
 * @property string $poster_path
 */
class Movie extends \yii\db\ActiveRecord
{
    const SCENARIO_FROM_MOVIEDB = 'save from moviedb';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'movie';
    }

//    public function scenarios()
//    {
//        $return = parent::scenarios();
//
//        $return[self::SCENARIO_FROM_MOVIEDB] = [
//            'id', 'release_date', 'runtime', 'overview', 'title', 'original_title', 'poster_path',
//        ];
//
//        return $return;
//    }

    public function fields()
    {
        return array_merge(parent::fields(), [
            'movie_id' => 'id',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['id', 'safe', 'on' => self::SCENARIO_FROM_MOVIEDB],
            [['release_date'], 'date', 'format' => 'yyyy-M-d'],
            [['runtime'], 'integer'],
            [['overview'], 'string'],
            [['title', 'original_title', 'poster_path'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'             => 'ID',
            'title'          => 'Title',
            'original_title' => 'Original Title',
            'release_date'   => 'Release Date',
            'runtime'        => 'Runtime',
            'overview'       => 'Overview',
//            'genres'         => 'Genres',
            'poster_path'    => 'Poster Path',
        ];
    }

    public function getGenre()
    {
        $this->hasMany(Genre::class, ['id' => 'movie_id'])->viaTable('movie_genre', ['genre_id' => 'id']);
    }
}
