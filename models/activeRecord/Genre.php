<?php

namespace app\models\activeRecord;

use Yii;

/**
 * This is the model class for table "genre".
 *
 * @property int $id
 * @property string $title
 */
class Genre extends \yii\db\ActiveRecord
{
    const SCENARIO_FROM_MOVIEDB = 'save from moviedb';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'genre';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['id','safe','on' => self::SCENARIO_FROM_MOVIEDB],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }
}
