<?php

namespace app\models\moviedb;

use app\components\themoviedb\response\MovieSearch;
use app\models\activeRecord\Movie;
use yii\base\Arrayable;
use yii\data\BaseDataProvider;
use Yii;

class MovieDataProvider extends BaseDataProvider
{
    /**
     * @var string|callable name of the key column or a callable returning it
     */
    public $key = 'id';

    /**
     * @param MovieSearch|null $searchResult
     *
     * @return $this
     */
    public function prepareBySearchMovie(MovieSearch $searchResult = null)
    {
        if ($searchResult) {
            $this->setMovieModels($searchResult->models);
            $this->setTotalCount(
                $searchResult->totalCount
            );

            if ($pagination = $this->getPagination()) {
                $pagination->page = $searchResult->page - 1;
                $pagination->pageSize = $searchResult->pageSize;
                $pagination->totalCount = $searchResult->totalCount;
            }
        }

        return $this;
    }


    /**
     * @param array $models
     *
     * @return $this
     */
    public function setMovieModels(array $models = [])
    {
        $results = [];

        foreach ($models as $key => $model) {

            $movieModel = Yii::createObject(['class' => Movie::class]);
            if ($model instanceof Arrayable) {
                $movieModel->setScenario(Movie::SCENARIO_FROM_MOVIEDB);
                $movieModel->load($model->toArray(), '');
                $movieModel->refresh();
            }

            $results[$key] = $movieModel;
        }

        $this->setModels($results);

        return $this;
    }


    /**
     * {@inheritdoc}
     */
    protected function prepareModels()
    {
        return $this->getModels();
    }

    /**
     * {@inheritdoc}
     */
    protected function prepareKeys($models)
    {
        if ($this->key !== null) {
            $keys = [];

            foreach ($models as $model) {
                if (is_string($this->key)) {
                    $keys[] = $model[$this->key];
                } else {
                    $keys[] = call_user_func($this->key, $model);
                }
            }

            return $keys;
        } else {
            return array_keys($models);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function prepareTotalCount()
    {
        return count($this->models);
    }
}