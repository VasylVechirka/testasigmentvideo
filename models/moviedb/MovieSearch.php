<?php

namespace app\models\moviedb;

use app\models\activeRecord\Genre as GenreActiveRecord;
use \app\models\activeRecord\Movie as MovieActiveRecord;
use Yii;

class MovieSearch extends AbstractMovieDBModel
{
    const SCENARIO_LIST = 'list';
    const SCENARIO_ONE = 'one';

    const TYPE_POPULAR = 'popular';
    const TYPE_DISCOVER = 'discover';

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $sort_field;

    /**
     * @var integer
     */
    public $sort_type;

    /**
     * @var integer
     */
    public $page;

    /**
     * @var string
     */
    public $type = self::TYPE_POPULAR;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'sort_field', 'sort_type', 'type'], 'safe', 'on' => self::SCENARIO_DEFAULT],
            ['id', 'required', 'on' => self::SCENARIO_ONE],
            [['sort_field', 'type'], 'string', 'on' => self::SCENARIO_LIST],
            [['page', 'sort_type'], 'integer', 'on' => self::SCENARIO_LIST],
            ['sortBy', 'safe', 'on' => self::SCENARIO_LIST], //не правильно
            ['type', 'in', 'range' => [self::TYPE_POPULAR, self::TYPE_DISCOVER], 'on' => self::SCENARIO_LIST],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function requestAttributes()
    {
        $attributes = parent::requestAttributes();

        if ($this->scenario == self::SCENARIO_LIST) {
            $attributes[] = 'sort_by';
        }

        return $attributes;
    }

    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        $fields = parent::fields();

        if ($this->scenario == self::SCENARIO_LIST) {
            $fields['sort_by'] = 'sortBy';
        }

        return $fields;
    }

    /**
     * @return string
     */
    protected function getSortBy()
    {
        $sort = (string)$this->sort_field;

        if ($sort && isset($this->sort_type)) {
            $sort = $this->sort_type === SORT_DESC ? "{$sort}.desc" : "{$sort}.asc";
        }

        return $sort;
    }

    /**
     * @param $data
     * @param null $formName
     *
     * @return MovieDataProvider
     */
    public function search($data, $formName = null)
    {
        $this->setScenario(self::SCENARIO_LIST);
        $this->load($data, $formName);

        $formName = strtolower($this->formName());
        /**
         * @var MovieDataProvider $dataProvide
         */
        $dataProvider = Yii::createObject([
            'class'      => MovieDataProvider::class,
            'pagination' => [
                'pageParam' => "{$formName}-page",
            ],
        ]);

        if ($this->type == self::TYPE_DISCOVER) {
            $dataProvider->setSort([
                'sortParam'  => "{$formName}-sort",
                'attributes' => [
                    "popularity"           => [
                        'default' => SORT_DESC,
                        'label'   => 'Popularity',
                    ],
                    "release_date"         => ['label' => 'Release Date'],
                    'revenue'              => ['label' => 'Revenue'],
                    'primary_release_date' => ['label' => 'Primary Release Date'],
                    'original_title'       => ['label' => 'Original title'],
                    'vote_average'         => ['label' => 'Vote average'],
                    'vote_count'           => ['label' => 'Vote Count'],

                ],
            ]);

            $sort = $dataProvider->getSort()->getAttributeOrders(true);

            if ($sort && is_null($this->sort_field)) {
                $this->sort_field = key($sort);
            }

            if ($sort && is_null($this->sort_type)) {
                $this->sort_type = current($sort);
            }
        }

        //Так делать не получается
//        $page = $dataProvider->getPagination()->getPage(true);

        $page = (int)Yii::$app->getRequest()->getQueryParam($dataProvider->getPagination()->pageParam, 0);
        if (is_null($this->page) && $page) {
            $this->page = $page;
        }

        if ($this->validate()) {

            $responseModel = null;

            switch ($this->type) {
                case self::TYPE_POPULAR:
                    $responseModel = $this->getMovieDBService()->getPopular($this->toRequestArray());
                    break;
                case self::TYPE_DISCOVER:
                    $responseModel = $this->getMovieDBService()->getDiscoverMovie($this->toRequestArray());
                    break;
            }

            $responseModel->validate();
            $dataProvider->prepareBySearchMovie($responseModel);

            $this->addErrors($responseModel->getErrors());
        }

        return $dataProvider;
    }

    /**
     * @param $data
     * @param null $formName
     * @param bool $withSave
     *
     * @return MovieActiveRecord|null
     */
    public function findOne($data, $formName = null, $withSave = true)
    {
        $this->setScenario(self::SCENARIO_ONE);
        $this->load($data, $formName);

        $activeRecordModel = MovieActiveRecord::findOne($this->id);

        if (!$activeRecordModel && $this->validate()) {
            $responseModel = $this->getMovieDBService()->getMovieDetails($this->toRequestArray());

            $validate = $responseModel->validate();

            $activeRecordModel = new MovieActiveRecord(['scenario' => MovieActiveRecord::SCENARIO_FROM_MOVIEDB]);
            $responseArray = $responseModel->toArray();
            $activeRecordModel->load($responseArray, '');

            if ($validate && $withSave) {
                $activeRecordModel->save();
            } elseif (!$validate) {
                $activeRecordModel->addErrors($responseModel->getErrors());
            }
        }

        if (!$this->validate()) {
            $activeRecordModel->addErrors($this->getErrors());
        }

        return $activeRecordModel;
    }
}