<?php
/**
 * Created by PhpStorm.
 * User: vechirka
 * Date: 16.06.18
 * Time: 2:07
 */

namespace app\models\moviedb;

use app\models\activeRecord\Movie as MovieActiveRecord;

class RatingForm extends AbstractMovieDBModel
{
    const SCENARIO_SAVE = 'rating save';

    /**
     * @var integer
     */
    public $movie_id;

    /**
     * @var double
     */
    public $value;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['movie_id', 'required'],
            ['movie_id', 'exist', 'targetClass' => MovieActiveRecord::class, 'targetAttribute' => ['movie_id' => 'id']],
            ['value', 'double', 'min' => 0.5, 'max' => 10],
            ['value', 'required', 'on' => self::SCENARIO_SAVE],
        ];
    }


    /**
     * @param $data
     * @param string|null $formName
     *
     * @return bool
     */
    public function save($data, $formName = null)
    {
        $this->setScenario(self::SCENARIO_SAVE);
        $this->load($data, $formName);

        if ($this->validate()) {
            $responseModel = $this->getMovieDBService()->setRating($this->toRequestArray());
            $validate = $responseModel->validate();

            if ($validate) {
                return true;
            } else {
                $this->addErrors($responseModel->getErrors());
            }
        }

        return false;
    }
}