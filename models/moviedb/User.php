<?php

namespace app\models\moviedb;

use yii\base\Model;
use yii\web\IdentityInterface;
use Yii;

class User extends Model implements IdentityInterface
{
    public $username = 'User!';

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $api_key;

    /**
     * @var string
     */
    public $guest_session_id;

    /**
     * @var string
     */
    public $expires_at;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'api_key', 'guest_session_id', 'expires_at'], 'required'],
            ['expires_at', 'validateExpired'],
        ];
    }

    /**
     * @param string $attribute
     *
     * @return bool
     */
    public function validateExpired($attribute)
    {
        if (strtotime('now') > strtotime($this->{$attribute})) {
            $this->addError($attribute, 'Time expired.');
        }

        return strtotime($this->expires_at) > strtotime('now');
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        $session = Yii::$app->session;

        if ($session->isActive && $session->has('movie-db-' . md5($id))) {
            $model = $session->get('movie-db-' . md5($id));

            if ($model instanceof User && $model->validate()) {
                return $model;
            }
        }

        return null;
    }

    /**
     * @return $this
     */
    public function saveToSession()
    {
        Yii::$app->session->set('movie-db-' . md5($this->id), $this);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return Yii::$app->security->generatePasswordHash($this->id);
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return Yii::$app->security->validatePassword($authKey, $this->id);
    }

}