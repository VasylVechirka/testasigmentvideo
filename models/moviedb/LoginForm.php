<?php

namespace app\models\moviedb;

use app\components\themoviedb\contract\ResponseModelAbstract;
use yii\base\Model;
use Yii;

class LoginForm extends Model
{
    public $auth_key;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['auth_key'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // auth_key is validated by validateMovieDB()
            ['auth_key', 'validateMovieDB'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateMovieDB($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user) {
                $this->addError($attribute, 'user not found!');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), 3600 * 24 * 30);
        }

        return false;
    }

    /**
     * Finds user by [[auth_key]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {

            /**
             * @var ResponseModelAbstract $responseModel
             */
            $responseModel = Yii::$app->themoviedb->auth(['api_key' => $this->auth_key]);

            if ($responseModel instanceof ResponseModelAbstract) {
                if ($responseModel->validate()) {

                    $model = new User();
                    $model->load(array_merge($responseModel->toArray(), [
                        'id'      => Yii::$app->security->generateRandomKey(),
                        'api_key' => $this->auth_key,
                    ]), '');

                    $this->_user = $model;
                    $model->saveToSession();
                } else {
                    $this->addErrors($responseModel->getErrors());
                }
            }
        }

        return $this->_user;
    }
}