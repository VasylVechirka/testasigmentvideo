<?php

namespace app\models\moviedb;


use app\components\themoviedb\ThemoviedbService;
use yii\base\Model;
use Yii;

abstract class AbstractMovieDBModel extends Model
{
    /**
     * @return ThemoviedbService
     */
    protected function getMovieDBService()
    {
        return Yii::$app->themoviedb;
    }

    /**
     * @return array
     */
    public function getMovieAuthParams()
    {
        $identity = Yii::$app->user->getIdentity();

        return $identity && $identity instanceof User ? $identity->toArray(['api_key', 'guest_session_id']) : [];
    }

    /**
     * @return array
     */
    protected function requestAttributes()
    {
        return $this->safeAttributes();
    }

    /**
     * @return array
     */
    protected function toRequestArray()
    {
        return array_merge($this->getMovieAuthParams(), $this->toArray($this->requestAttributes()));
    }
}