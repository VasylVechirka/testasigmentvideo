<?php

namespace app\components\themoviedb;


use app\components\themoviedb\contract\RequestFactoryInterface;
use app\components\themoviedb\contract\RequestModelInterface;
use app\components\themoviedb\contract\ResponseModelAbstract;
use app\components\themoviedb\response\Auth;
use app\components\themoviedb\response\Movie;
use app\components\themoviedb\response\MovieSearch;
use yii\base\Component;

class ThemoviedbService extends Component
{
    /**
     * @var RequestFactoryInterface
     */
    protected $requestFactory;

    /**
     * @var array
     */
    public $baseParams = [];

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setRequestFactory($type)
    {
        $factory = null;

        switch ($type) {
            case 'http':
            default:
                $factory = new HttpRequestFactory();
                break;
        }

        $this->requestFactory = $factory;

        return $this;
    }

    /**
     * @param array $data
     *
     * @return Auth
     */
    public function auth(array $data = [])
    {
        $model = $this->requestFactory
            ->auth($this->baseParams)
            ->setRequestData($data);

        return $this->doAction($model);
    }

    /**
     * @param array $data
     *
     * @return MovieSearch
     */
    public function getDiscoverMovie(array $data = [])
    {
        $model = $this->requestFactory
            ->discoverMovie($this->baseParams)
            ->setRequestData($data);

        return $this->doAction($model);
    }

    /**
     * @param array $data
     *
     * @return MovieSearch
     */
    public function getPopular(array $data = [])
    {
        $model = $this->requestFactory
            ->popular($this->baseParams)
            ->setRequestData($data);

        return $this->doAction($model);
    }

    /**
     * @param array $data
     *
     * @return Movie
     */
    public function getMovieDetails(array $data = [])
    {
        $model = $this->requestFactory
            ->movieDetails($this->baseParams)
            ->setRequestData($data);


        return $this->doAction($model);
    }


    /**
     * @param array $data
     *
     * @return Movie
     */
    public function setRating(array $data = [])
    {
        $model = $this->requestFactory
            ->setRating($this->baseParams)
            ->setRequestData($data);

        return $this->doAction($model);
    }


    /**
     * @param RequestModelInterface $model
     *
     * @return ResponseModelAbstract
     */
    protected function doAction(RequestModelInterface $model)
    {
        if ($model->validate()) {
            $model->execute();
        } else {
            $model->getResponseModel()->request_errors = $model->getValidateErrors();
        }

        return $model->getResponseModel();
    }
}