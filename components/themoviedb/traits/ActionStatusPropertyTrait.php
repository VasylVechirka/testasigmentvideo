<?php
/**
 * Created by PhpStorm.
 * User: vechirka
 * Date: 16.06.18
 * Time: 23:47
 */

namespace app\components\themoviedb\traits;


use app\components\themoviedb\response\ActionStatus;

trait ActionStatusPropertyTrait
{
    /**
     * @var ActionStatus
     */
    public $action_status;

    /**
     * @param $attribute
     *
     * @return bool
     */
    public function validateActionStatus($attribute)
    {
        if ($this->action_status && !$this->action_status->validate()) {
            $this->addErrors([$attribute => $this->action_status->getErrorSummary(true)]);

            return false;
        }

        return true;
    }

    /**
     * @param array $mergeParams
     *
     * @return array
     */
    protected function actionStatusRules(array $mergeParams = [])
    {
        return array_merge([['action_status'], 'validateActionStatus'], $mergeParams);
    }
}