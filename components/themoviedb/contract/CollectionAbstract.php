<?php

namespace app\components\themoviedb\contract;


use yii\base\Model;

abstract class CollectionAbstract extends \Tightenco\Collect\Support\Collection
{
    const ITEM_CLASS = '';

    public function __construct($items = [])
    {
        $availableItems = [];
        if ($items) {
            foreach ($items as $item) {
                if ($this->checkItem($item)) {
                    $availableItems[] = $item;
                }
            }
        }
        parent::__construct($availableItems);
    }

    public function offsetSet($key, $value)
    {
        if ($this->checkItem($value)) {
            parent::offsetSet($key, $value);
        }

        return $this;
    }

    public function add($data = null, $safeOnly = true)
    {
        if ($data) {
            $class = $this->getItemClass();

            $item = new $class();
            if ($item instanceof Model) {
                $item->setAttributes($data, $safeOnly);
            }


            $this->push($item);
        }
    }

    public function massAdd(array $data = [], $safeOnly = true)
    {
        if ($data) {
            foreach ($data as $item) {
                $this->add($item, $safeOnly);
            }
        }

        return $this;
    }

    private function checkItem($item)
    {
        return is_a($item, $this->getItemClass());
    }

    public function getItemClass()
    {
        return static::ITEM_CLASS;
    }
}