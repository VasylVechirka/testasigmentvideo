<?php

namespace app\components\themoviedb\contract;

use app\components\themoviedb\response\ActionStatus;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use yii\base\Model;
use Yii;

abstract class RequestHttpAbstract extends Model implements RequestModelInterface
{
    const METHOD = 'GET';
    const URI = '';

    /**
     * @var string
     */
    public $api_key;

    /**
     * @var Client;
     */
    protected $requestClient;

    /**
     * @var \Psr\Http\Message\ResponseInterface
     */
    protected $requestResponse;

    /**
     * @var ResponseHandlerInterface
     */
    protected $handler;

    /**
     * @var ResponseModelAbstract
     */
    protected $responseModel;


    public function __construct(array $config = [])
    {
        $baseUri = '';
        if (isset($config['baseUri'])) {
            $baseUri = $config['baseUri'];
            unset($config['baseUri']);
        }

        parent::__construct($config);

        $this->requestClient = new Client(['base_uri' => $baseUri]);
    }

    /**
     * @param ResponseModelAbstract $model
     *
     * @return $this
     */
    public function setResponseModel(ResponseModelAbstract $model)
    {
        $this->responseModel = $model;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseModel()
    {
        return $this->responseModel;
    }

    /**
     * @return ResponseHandlerInterface
     */
    public function getHandler()
    {
        return $this->handler;
    }

    /**
     * @param ResponseHandlerInterface $handler
     *
     * @return $this
     */
    public function setHandler($handler)
    {
        $this->handler = $handler;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['api_key', 'required'],
        ];
    }

    /**
     * @return string
     */
    public function getRequestUri()
    {
        return static::URI;
    }


    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function getRequest()
    {
        return $this->requestClient->get($this->getRequestUri(), [
            'query'           => $this->getQueryParams(),
            'allow_redirects' => false,
        ]);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function postRequest()
    {
        return $this->requestClient->post($this->getRequestUri(), [
            'form_params'     => $this->getFormParams(),
            'query'           => $this->getQueryParams(),
            'allow_redirects' => false,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setRequestData(array $data)
    {
        $this->load($data, '');

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getValidateErrors()
    {
        return $this->getErrors();
    }

    /**
     * @return array
     */
    public function getQueryParams()
    {
        return array_filter($this->toArray());
    }

    /**
     * @return array
     */
    public function getFormParams()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $method = static::METHOD;
        $response = null;

        try {
            $response = $this->{"{$method}Request"}();
        } catch (ClientException $e) {
            $response = $response = $e->getResponse();
        } catch (ConnectException $e) {
            $response = $response = $e->getResponse();
            Yii::warning('TheMovieDb Service Request ConnectException: <![CDATA[' . $e->getMessage() . ']]>');
        } catch (RequestException $e) {
            $response = $response = $e->getResponse();
            Yii::warning('TheMovieDb Service Request RequestException: <![CDATA[' . $e->getMessage() . ']]>');
        }

        $this->handler->setRequestResponse($response);

        return $this->loadToResponseModel($this->handler->handle());
    }

    /**
     * @param array $response
     *
     * @return $this
     */
    protected function loadToResponseModel(array $response = [])
    {
        if (isset($response['action_status'])) {
            $response['action_status'] = (new ActionStatus($response['action_status']));
        }

        if ($this->responseModel) {
            $this->responseModel->setScenarioFromResponse();
            $this->responseModel->load($response, '');
        }

        return $this;
    }
}