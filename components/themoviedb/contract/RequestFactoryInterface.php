<?php

namespace app\components\themoviedb\contract;


interface RequestFactoryInterface
{
    /**
     * @param array $params
     *
     * @return RequestModelInterface
     */
    public function auth(array $params = []);

    /**
     * @param array $params
     *
     * @return RequestModelInterface
     */
    public function discoverMovie(array $params = []);

    /**
     * @param array $params
     *
     * @return RequestModelInterface
     */
    public function popular(array $params = []);

    /**
     * @param array $params
     *
     * @return RequestModelInterface
     */
    public function setRating(array $params = []);

    /**
     * @param array $params
     *
     * @return RequestModelInterface
     */
    public function movieDetails(array $params = []);


}