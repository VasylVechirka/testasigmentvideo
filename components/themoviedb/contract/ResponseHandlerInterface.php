<?php
/**
 * Created by PhpStorm.
 * User: vechirka
 * Date: 17.06.18
 * Time: 2:22
 */

namespace app\components\themoviedb\contract;


interface ResponseHandlerInterface
{
    /**
     * @param mixed $response
     *
     * @return $this
     */
    public function setRequestResponse($response = null);

    /**
     * @return mixed
     */
    public function getRequestResponse();

    /**
     * @return mixed
     */
    public function handle();

}