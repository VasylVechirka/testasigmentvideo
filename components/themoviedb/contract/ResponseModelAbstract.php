<?php

namespace app\components\themoviedb\contract;

use yii\base\Model;

abstract class ResponseModelAbstract extends Model
{
    const SCENARIO_FROM_RESPONSE = 'from response';

    /**
     * @var string
     */
    public $status_message;
    /**
     * @var integer
     */
    public $status_code;
    /**
     * @var array
     */
    public $response_errors;

    /**
     * @var array
     */
    public $request_errors;

    /**
     * @var string
     */
    public $responseStatusCode;

    /**
     * @return $this
     */
    public function setScenarioFromResponse()
    {
        $this->setScenario(static::SCENARIO_FROM_RESPONSE);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['request_errors', 'validateErrorsMustBeEmpty'],
            ['response_errors', 'validateErrorsMustBeEmpty', 'on' => self::SCENARIO_FROM_RESPONSE],
            'responseStatusCode' => [['responseStatusCode'], 'required', 'requiredValue' => 200],
        ];
    }

    /**
     * @param $attribute
     *
     * @return bool
     */
    public function validateErrorsMustBeEmpty($attribute)
    {
        if (!empty($this->$attribute)) {
            $this->addErrors($this->$attribute);

            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function checkResponseError()
    {
        return $this->validate(['status_message', 'response_errors']);
    }
}