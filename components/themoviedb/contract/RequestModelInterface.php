<?php

namespace app\components\themoviedb\contract;


interface RequestModelInterface
{
    /**
     * @param array $data
     *
     * @return $this
     */
    public function setRequestData(array $data);

    /**
     * @return boolean
     */
    public function validate();

    /**
     * @return array
     */
    public function getValidateErrors();

    /**
     * @return ResponseModelAbstract
     */
    public function getResponseModel();

    /**
     * @return $this
     */
    public function execute();
}