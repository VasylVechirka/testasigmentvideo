<?php
/**
 * Created by PhpStorm.
 * User: vechirka
 * Date: 15.06.18
 * Time: 22:32
 */

namespace app\components\themoviedb\request\http;

use app\components\themoviedb\contract\RequestHttpAbstract;

class Rating extends RequestHttpAbstract
{
    const URI = 'movie/{movie_id}}/rating';
    const METHOD = 'POST';

    /**
     * @var integer
     */
    public $movie_id;

    /**
     * @var string
     */
    public $guest_session_id;

    /**
     * @var double
     */
    public $value;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['movie_id', 'value'], 'required'],
            ['guest_session_id', 'string'],
            ['value', 'double', 'min' => 0.5, 'max' => 10],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getFormParams()
    {
        return array_filter($this->toArray(['value']));
    }

    /**
     * {@inheritdoc}
     */
    public function getQueryParams()
    {
        return array_filter($this->toArray([], ['value']));
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestUri()
    {
        return str_replace('{movie_id}', $this->movie_id, parent::getRequestUri());
    }

    /**
     * {@inheritdoc}
     */
    protected function loadToResponseModel(array $response = [])
    {
        if (isset($response['action_status'])) {
            $response = array_merge($response, $response['action_status']);
        }

        return parent::loadToResponseModel($response);
    }
}