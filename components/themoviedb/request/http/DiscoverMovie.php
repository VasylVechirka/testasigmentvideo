<?php

namespace app\components\themoviedb\request\http;

use app\components\themoviedb\contract\RequestHttpAbstract;

class DiscoverMovie extends RequestHttpAbstract
{
    const URI = 'discover/movie';

    /**
     * @var string
     */
    public $language;

    /**
     * @var string
     */
    public $sort_by;

    /**
     * @var integer
     */
    public $page;

    /**
     * @var string
     */
    public $release_after;

    /**
     * @var string
     */
    public $release_before;

    /**
     * :)
     * @var bool
     */
    public $include_adult = true;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['language','default','value'=>'en-EN'],
            [['language'], 'match', 'pattern' => '/^([a-z]{2})-([A-Z]{2})$/i'],
            ['release_after', 'default', 'value' => (new \DateTime('now'))->modify('- 2 months')->format('Y-m-d')],
            ['release_after', 'date', 'format' => 'Y-m-d'],

            ['release_before', 'default', 'value' => (new \DateTime('now'))->modify('+ 2 months')->format('Y-m-d')],
            ['release_before', 'date', 'format' => 'Y-m-d'],

            ['page', 'integer', 'min' => 1, 'max' => 1000],
            [['include_adult'], 'boolean'],
            ['sort_by', 'in', 'range' => [
                'popularity.asc',
                'popularity.desc',
                'release_date.asc',
                'release_date.desc',
                'revenue.asc',
                'revenue.desc',
                'primary_release_date.asc',
                'primary_release_date.desc',
                'original_title.asc',
                'original_title.desc',
                'vote_average.asc',
                'vote_average.desc',
                'vote_count.asc',
                'vote_count.desc',
            ]],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        return array_merge(parent::fields(), [
            'primary_release_date.lte' => 'release_before',
            'primary_release_date.gte' => 'release_after',
        ]);
    }
}