<?php

namespace app\components\themoviedb\request\http;


use app\components\themoviedb\contract\RequestHttpAbstract;
use app\components\themoviedb\response\Movie;

class MovieDetails extends RequestHttpAbstract
{
    const URI = 'movie/{id}';

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $language;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['id', 'required'],
            [['language'], 'match', 'pattern' => '/^[a-z]{2})-([A-Z]{2}$/i'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestUri()
    {
        return str_replace('{id}', $this->id, parent::getRequestUri());
    }
}