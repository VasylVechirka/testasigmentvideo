<?php

namespace app\components\themoviedb\request\http;

use app\components\themoviedb\contract\RequestHttpAbstract;

class Authentication extends RequestHttpAbstract
{
    const URI = 'authentication/guest_session/new';
}