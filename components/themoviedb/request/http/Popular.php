<?php

namespace app\components\themoviedb\request\http;

use app\components\themoviedb\contract\RequestHttpAbstract;

class Popular extends RequestHttpAbstract
{
    const URI = 'movie/popular';

    /**
     * @var string
     */
    public $language;

    /**
     * @var integer
     */
    public $page;

    /**
     * @var string
     */
    public $region;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['language','default','value'=>'en-EN'],
            [['language'], 'match', 'pattern' => '/^([a-z]{2})-([A-Z]{2})$/i'],
            ['page', 'integer', 'min' => 1, 'max' => 1000],
            ['region', 'match', 'pattern' => '/^[A-Z]{2}$/i'],
        ]);
    }
}