<?php
/**
 * Created by PhpStorm.
 * User: vechirka
 * Date: 17.06.18
 * Time: 2:26
 */

namespace app\components\themoviedb\handler;

use app\components\themoviedb\contract\RequestModelInterface;
use app\components\themoviedb\response\ActionStatus;
use \Psr\Http\Message\ResponseInterface;
use app\components\themoviedb\contract\ResponseHandlerInterface;
use app\components\themoviedb\contract\ResponseModelAbstract;

class HttpResponseHandler implements ResponseHandlerInterface
{

    /**
     * @var ResponseInterface
     */
    protected $requestResponse;

    /**
     * @var array
     */
    protected $response = [];

    /**
     * @param ResponseInterface $response
     *
     * @return $this
     */
    public function setRequestResponse($response = null)
    {
        $this->requestResponse = $response instanceof ResponseInterface ? $response : null;

        return $this;
    }

    /**
     * @return ResponseInterface|null
     */
    public function getRequestResponse()
    {
        return $this->requestResponse;
    }

    /**
     * Prepare response data for @see ResponseModelAbstract
     *
     * @return array
     */
    public function handle()
    {
        $requestResponse = $this->requestResponse;
        $response = $requestResponse ? $requestResponse->getBody()->getContents() : null;

        if ($response) {
            $response = json_decode(trim($response), true);
        }
        $response = $this->handleArrayData($response ?: [], true);
        $response['responseStatusCode'] = $requestResponse ? $requestResponse->getStatusCode() : null;

        return $response;
    }

    /**
     * @param array $response
     * @param bool $withoutNullResponse
     *
     * @return array
     */
    public function handleArrayData(array $response = [], $withoutNullResponse = true)
    {
        if (!$response && $withoutNullResponse) {
            $response = [
                'status_code'    => 0,
                'status_message' => 'null response',
            ];
        }

        if ($response) {

            if (isset($response['errors'])) {
                $response['response_errors'] = $response['errors'];
                unset($response['errors']);
            }

            $actionsStatus = [];
            if (isset($response['status_message'])) {
                $actionsStatus['status_message'] = $response['status_message'];
                unset($response['status_message']);

            }

            if (isset($response['status_code'])) {
                $actionsStatus['status_code'] = $response['status_code'];
                unset($response['status_code']);
            }

            if ($actionsStatus) {
                $response['action_status'] = $actionsStatus;
            }
        }

        return $response;
    }
}