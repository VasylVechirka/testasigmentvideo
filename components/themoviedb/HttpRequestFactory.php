<?php

namespace app\components\themoviedb;


use app\components\themoviedb\contract\RequestFactoryInterface;
use app\components\themoviedb\handler\HttpResponseHandler;
use app\components\themoviedb\request\http\Authentication;
use app\components\themoviedb\request\http\DiscoverMovie;
use app\components\themoviedb\request\http\MovieDetails;
use app\components\themoviedb\request\http\Popular;
use app\components\themoviedb\request\http\Rating;
use app\components\themoviedb\response\ActionStatus;
use app\components\themoviedb\response\Auth as AuthenticationResponse;
use app\components\themoviedb\response\MovieSearch as MovieSearchResponse;
use app\components\themoviedb\response\Movie as MovieDetailsResponse;
use Yii;

class HttpRequestFactory implements RequestFactoryInterface
{
    /**
     * @param array $params
     *
     * @return Authentication
     */
    public function auth(array $params = [])
    {
        return Yii::createObject(array_merge($params, [
            'class'         => Authentication::class,
            'handler'       => Yii::createObject(HttpResponseHandler::class),
            'responseModel' => Yii::createObject(AuthenticationResponse::class),
        ]));
    }

    /**
     * @param array $params
     *
     * @return DiscoverMovie
     */
    public function discoverMovie(array $params = [])
    {
        return Yii::createObject(array_merge($params, [
            'class'         => DiscoverMovie::class,
            'handler'       => Yii::createObject(HttpResponseHandler::class),
            'responseModel' => Yii::createObject(MovieSearchResponse::class),
        ]));
    }

    /**
     * @param array $params
     *
     * @return Popular
     */
    public function popular(array $params = [])
    {
        return Yii::createObject(array_merge($params, [
            'class'         => Popular::class,
            'handler'       => Yii::createObject(HttpResponseHandler::class),
            'responseModel' => Yii::createObject(MovieSearchResponse::class),
        ]));
    }

    /**
     * @param array $params
     *
     * @return MovieDetails
     */
    public function movieDetails(array $params = [])
    {
        return Yii::createObject(array_merge($params, [
            'class'         => MovieDetails::class,
            'handler'       => Yii::createObject(HttpResponseHandler::class),
            'responseModel' => Yii::createObject(MovieDetailsResponse::class),
        ]));
    }

    /**
     * @param array $params
     *
     * @return Rating
     */
    public function setRating(array $params = [])
    {
        return Yii::createObject(array_merge($params, [
            'class'         => Rating::class,
            'handler'       => Yii::createObject(HttpResponseHandler::class),
            'responseModel' => Yii::createObject(ActionStatus::class),
        ]));
    }
}