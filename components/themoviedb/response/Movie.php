<?php

namespace app\components\themoviedb\response;

use app\components\themoviedb\contract\ResponseModelAbstract;
use app\components\themoviedb\response\collection\GenreCollection;
use app\components\themoviedb\traits\ActionStatusPropertyTrait;

/**
 * Class Movie
 *
 * @property GenreCollection $genres
 *
 * @package app\components\themoviedb\response
 */
class Movie extends ResponseModelAbstract
{
    use ActionStatusPropertyTrait;

    /**
     * @var integer
     */
    public $id;
    /**
     * @var string
     */
    public $video;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $original_title;
    /**
     * @var string
     */
    public $release_date;
    /**
     * @var integer
     */
    public $runtime;
    /**
     * @var string
     */
    public $overview;
    /**
     * @var string
     */
    public $poster_path;

    /**
     * @var GenreCollection
     */
    protected $_genres;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['id', 'video', 'title', 'original_title', 'release_date', 'runtime', 'overview', 'poster_path', 'genres'], 'safe'],
                $this->actionStatusRules(['on' => self::SCENARIO_FROM_RESPONSE]),
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), ['genres']);
    }

    /**
     * @param array $data
     *
     * @return $this
     */
    public function setGenres(array $data = [])
    {
        $this->_genres = (new GenreCollection())->massAdd($data, false);

        return $this;
    }

    /**
     * @return GenreCollection
     */
    public function getGenres()
    {
        return $this->_genres;
    }
}