<?php
/**
 * Created by PhpStorm.
 * User: vechirka
 * Date: 15.06.18
 * Time: 22:36
 */

namespace app\components\themoviedb\response;

use app\components\themoviedb\contract\ResponseModelAbstract;
use app\components\themoviedb\traits\ActionStatusPropertyTrait;

class ActionStatus extends ResponseModelAbstract
{
    use ActionStatusPropertyTrait;
    /**
     * @var integer
     */
    public $status_code;

    /**
     * @var string
     */
    public $status_message;

    /**
     * @return string
     */
    public function __toString()
    {
        $string = [];

        if (!is_null($this->status_code)) {
            $string[] = $this->status_code;
        }

        if (!is_null($this->status_message)) {
            $string[] = $this->status_message;
        }

        return implode(' : ', $string);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                'isValid',
                'required',
                'message'       => $this->__toString(),
                'strict'        => true,
                'requiredValue' => true,
                'on'            => self::SCENARIO_DEFAULT,
            ],
            [['status_message', 'status_code'], 'safe', 'on' => self::SCENARIO_FROM_RESPONSE],
            'responseStatusCode' => [['responseStatusCode'], 'required', 'requiredValue' => 201, 'on' => self::SCENARIO_FROM_RESPONSE],
            $this->actionStatusRules([
                'on'   => self::SCENARIO_FROM_RESPONSE,
                'when' => function () {
                    return $this->responseStatusCode != 201;
                }]),
        ]);
    }

    /**
     * @return bool
     */
    public function getIsValid()
    {
        return (is_null($this->status_code) && is_null($this->status_message)) ? true : false;
    }
}