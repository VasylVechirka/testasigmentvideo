<?php

namespace app\components\themoviedb\response\collection;

use app\components\themoviedb\contract\CollectionAbstract;
use app\components\themoviedb\response\Movie;

class MovieCollection extends CollectionAbstract
{
    const ITEM_CLASS = Movie::class;
}