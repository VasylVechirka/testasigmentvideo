<?php

namespace app\components\themoviedb\response\collection;

use app\components\themoviedb\contract\CollectionAbstract;
use app\components\themoviedb\response\Genre;

class GenreCollection extends CollectionAbstract
{
    const ITEM_CLASS = Genre::class;
}