<?php

namespace app\components\themoviedb\response;

use app\components\themoviedb\contract\ResponseModelAbstract;
use app\components\themoviedb\response\collection\MovieCollection;
use app\components\themoviedb\traits\ActionStatusPropertyTrait;

/**
 * Class MovieSearch
 *
 * @property MovieCollection $results
 * @property \app\models\activeRecord\Movie[] $models
 * @property integer $totalCount
 * @property integer $pageSize
 *
 * @package app\components\themoviedb\response
 */
class MovieSearch extends ResponseModelAbstract
{
    use ActionStatusPropertyTrait;

    const PAGE_SIZE = 20;
    const MAX_PAGES = 1000;

    /**
     * @var integer
     */
    public $page;

    /**
     * @var integer
     */
    public $total_results;

    /**
     * @var integer
     */
    public $total_pages;

    /**
     * @var MovieCollection
     */
    protected $_results;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['page', 'total_results', 'total_pages', 'results'], 'safe'],
            $this->actionStatusRules(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'results',
        ]);
    }

    /**
     * @param array $results
     *
     * @return $this
     */
    public function setResults(array $results = [])
    {
        $this->_results = (new MovieCollection())->massAdd($results, false);

        return $this;
    }

    /**
     * @return MovieCollection
     */
    public function getResults()
    {
        return $this->_results;
    }

    /**
     * @return integer
     */
    public function getTotalCount()
    {
        return min([
            $this->total_results,
            static::PAGE_SIZE * static::MAX_PAGES,
        ]);
    }

    /**
     * @return integer
     */
    public function getPageSize()
    {
        return static::PAGE_SIZE;
    }

    /**
     * @return array
     */
    public function getModels()
    {
        return $this->getResults() ? $this->getResults()->all() : [];
    }
}