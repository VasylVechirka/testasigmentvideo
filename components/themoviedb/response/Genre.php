<?php

namespace app\components\themoviedb\response;

use yii\base\Model;

class Genre extends Model
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [[['id','name'],'safe']];
    }
}