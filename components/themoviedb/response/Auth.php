<?php

namespace app\components\themoviedb\response;

use app\components\themoviedb\contract\ResponseModelAbstract;
use app\components\themoviedb\traits\ActionStatusPropertyTrait;

class Auth extends ResponseModelAbstract
{
    use ActionStatusPropertyTrait;

    /**
     * @var string
     */
    public $guest_session_id;

    /**
     * @var string
     */
    public $expires_at;

    /**
     * @var boolean
     */
    public $success;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['guest_session_id', 'expires_at', 'success'], 'safe'],
            ['success','required','requiredValue' => true],
            $this->actionStatusRules(),
        ]);
    }
}