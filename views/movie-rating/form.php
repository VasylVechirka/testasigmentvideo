<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/**
 * @var $this \yii\web\View
 * @var $model \app\models\moviedb\RatingForm
 * @var $form ActiveForm
 * @var $movieModel \app\models\activeRecord\Movie
 */

$this->title = 'Set Rating';

$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>

<div class="from">

    <?php $form = ActiveForm::begin([]); ?>

    <?= $form->field($model, 'value') ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php if ($model->getErrors()): ?>
        <div class="error-summary">
            <?= Html::errorSummary($model) ?>
        </div>
    <?php endif ?>

    <?php ActiveForm::end(); ?>
</div>


<div id="movie-view" class="index">
    <?php if ($movieModel->getErrors()): ?>
        <div class="error-summary">
            <?= Html::errorSummary($movieModel) ?>
        </div>
    <?php endif ?>


    <?= DetailView::widget([
        'model'      => $movieModel,
        'attributes' => [
            'id',
            'title',
            'original_title',
            'release_date',
            'runtime',
            'overview',
//            'genres',
            'poster_path',
        ],
    ]); ?>
</div>

