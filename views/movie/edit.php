<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\activeRecord\Movie */
/* @var $form ActiveForm */


$this->title = 'Movie edit';

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="from">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'release_date') ?>
        <?= $form->field($model, 'runtime') ?>
        <?= $form->field($model, 'overview') ?>
        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'original_title') ?>

        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>

    <?php if ($model->getErrors()): ?>
        <div class="error-summary">
            <?= Html::errorSummary($model) ?>
        </div>
    <?php endif ?>

    <?php ActiveForm::end(); ?>

</div>
