<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\moviedb\MovieSearch;
use yii\widgets\ActiveForm;
use yii\widgets\LinkSorter;

/**
 * @see \app\controllers\MovieController
 * @var $this           \yii\web\View
 * @var $dataProvider   \app\models\moviedb\MovieDataProvider
 * @var $searchModel   \app\models\moviedb\MovieSearch
 * @var $sort   \app\models\moviedb\MovieSearch
 */

$this->title = 'Movie List';

$this->params['breadcrumbs'][] = $this->title;

?>
<div id="movie-index" class="index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?php $form = ActiveForm::begin([
        'method' => 'get',
        'action' => ['movie/index'],
    ]); ?>

    <?= $form->field($searchModel, 'type')->dropDownList([
        MovieSearch::TYPE_POPULAR  => 'Popular',
        MovieSearch::TYPE_DISCOVER => 'Discover Search',
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>
    <?= $form->errorSummary($searchModel) ?>
    <?php ActiveForm::end(); ?>


    <?php if ($searchModel && $dataProvider->getSort() && $dataProvider->getSort()->attributes): ?>
        <div class="sort">
            <h2>Sorts</h2>
            <?= LinkSorter::widget(['sort' => $dataProvider->getSort()]) ?>
        </div>
    <?php endif ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'sorter'       => [],
        'columns'      => [
            'tools' => [
                'class'          => 'yii\grid\ActionColumn',
                'template'       => '{view} {delete} {update}',
                'visibleButtons' => [
                    'delete' => function ($model, $key, $index) {
                        /**
                         * @var \app\models\activeRecord\Movie $model
                         */
                        return !$model->getIsNewRecord();
                    },
                ],
            ],
            [
                'label'     => 'Set Rating',
                'format'    => 'raw',
                'attribute' => 'id',
                'value'     => function ($data) {
                    return Html::a('Set rating', ['movie-rating/set', 'movie_id' => $data->id]);
                },
            ],
            ['attribute' => 'id', 'enableSorting' => false],
            ['attribute' => 'title', 'enableSorting' => false],
            ['attribute' => 'release_date', 'enableSorting' => false],
        ],
        'pager'        => [
            'firstPageLabel' => 'First',
            'lastPageLabel'  => 'Last',
        ],
        'layout'       => "{summary}\n{pager}\n{items}\n{pager}",
    ]); ?>
</div>
