<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @see \app\controllers\MovieController
 * @var $this           \yii\web\View
 * @var $model   \app\models\activeRecord\Movie
 */

$this->title = 'Movie View Details';

$this->params['breadcrumbs'][] = $this->title;

?>
<div id="movie-view" class="index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'original_title',
            'release_date',
            'runtime',
            'overview',
//            'genres',
            'poster_path'
        ],
    ]); ?>

    <?= Html::errorSummary($model)?>
</div>
