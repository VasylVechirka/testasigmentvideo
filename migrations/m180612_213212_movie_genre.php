<?php

use yii\db\Migration;

/**
 * Class m180612_213212_movie_genre
 */
class m180612_213212_movie_genre extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('movie_genre', [
            'movie_id'=>$this->integer(),
            'genre_id'=>$this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180612_213212_movie_genre reverted.\n";

        $this->dropTable('movie_genre');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180612_213212_movie_genre cannot be reverted.\n";

        return false;
    }
    */
}
