<?php

use yii\db\Migration;

/**
 * Class m180608_091526_movie
 */
class m180608_091526_movie extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('movie', [
            'id'             => $this->primaryKey(),
            'title'          => $this->string(),
            'original_title' => $this->string(),
            'release_date'   => $this->date()->null(),
            'runtime'        => $this->integer(),
            'overview'       => $this->text(),
            'genres'         => $this->text(),
            'poster_path'    => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('movie');

        echo "m180608_091526_movie reverted.\n";

        return true;
    }
}
