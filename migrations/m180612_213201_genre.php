<?php

use yii\db\Migration;

/**
 * Class m180612_213201_genre
 */
class m180612_213201_genre extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('genre', [
            'id'    => $this->primaryKey(),
            'title' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('genre');

        echo "m180612_213201_genre reverted.\n";

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180612_213201_genre cannot be reverted.\n";

        return false;
    }
    */
}
