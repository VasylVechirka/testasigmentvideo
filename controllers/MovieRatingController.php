<?php
/**
 * Created by PhpStorm.
 * User: vechirka
 * Date: 16.06.18
 * Time: 1:44
 */

namespace app\controllers;


use app\models\moviedb\Rating;
use app\models\moviedb\RatingForm;
use yii\web\Controller;
use app\models\moviedb\MovieSearch;
use Yii;


class MovieRatingController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionSet($movie_id)
    {
        $ratingForm = (new RatingForm());
        $ratingForm->load(['movie_id' => $movie_id], '');


        if (Yii::$app->request->isPost && $ratingForm->save(Yii::$app->request->post())) {
            return $this->redirect(['movie/index']);
        }

        return $this->render('form', [
            'model'      => $ratingForm,
            'movieModel' => (new MovieSearch())->findOne(['id' => $movie_id], ''),
        ]);
    }
}