<?php

namespace app\controllers;

use app\models\activeRecord\Movie;
use app\models\moviedb\MovieDataProvider;
use app\models\moviedb\MovieSearch;
use yii\web\Controller;
use Yii;

class MovieController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        /**
         * @var MovieDataProvider $dataProvider
         * @var MovieSearch $model
         */
        $model = new MovieSearch();

        $dataProvider = $model->search(Yii::$app->request->get());

        return $this->render('index', [
            'searchModel'  => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => (new MovieSearch())->findOne(['id' => $id], ''),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = (new MovieSearch())->findOne(['id' => $id], '');

        if ($model->validate() && $model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        Movie::deleteAll($id);

        return $this->goBack(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }
}